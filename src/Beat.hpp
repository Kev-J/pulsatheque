/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#ifndef BEAT_HPP
#define BEAT_HPP

#include <filesystem>
#include <string>

#include "TimeSignature.hpp"

class Beat {
    public:
        Beat();

        std::string getName(void) const;
        double getTempo(void) const;
        unsigned int getLength(void) const;
        double getIntensity(void) const;
        const TimeSignature& getTimeSignature(void) const;
        const std::filesystem::path& getFilePath(void) const;
        std::string getHeader(void) const;

        void setName(const std::string& name);
        void setTempo(double tempo);
        void setLength(unsigned int length);
        void setIntensity(double length);
        void setTimeSignature(const TimeSignature& ts);
        void setFilePath(const std::filesystem::path& filePath);
        void setHeader(const std::string& header);

    private:
        std::string name;
        double tempo;
        unsigned int length;
        double intensity;
        TimeSignature timeSignature;
        std::filesystem::path filePath;
        std::string header;
};

#endif // BEAT_HPP

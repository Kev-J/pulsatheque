/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#ifndef BEAT_LIST_VIEW
#define BEAT_LIST_VIEW

#include <filesystem>

#include <gtkmm.h>

#include "TimeSignature.hpp"

class Beat;
class Library;
class Player;

class BeatColumnView : public Gtk::ScrolledWindow {
    public:
        BeatColumnView();
        virtual ~BeatColumnView();

        void setLibrary(const Library *library);
        void setPlayer(Player *player);

        bool playSelected(void);

    protected:

        class ModelColumns: public Glib::Object {
            public:
                Glib::ustring name;
                Glib::ustring header;
                double tempo;
                unsigned int length;
                TimeSignature timeSignature;
                double intensity;
                std::filesystem::path filepath;

                static Glib::RefPtr<ModelColumns> create(Glib::ustring name,
                                                         Glib::ustring header,
                                                         double tempo,
                                                         unsigned int length,
                                                         const TimeSignature& timeSignature,
                                                         double intensity,
                                                         const std::filesystem::path& filepath)
                {
                    return Glib::make_refptr_for_instance<ModelColumns>(new ModelColumns(name, header, tempo, length, timeSignature, intensity, filepath));
                }

            protected:
                ModelColumns(const Glib::ustring& name, const Glib::ustring& header, double tempo, unsigned int length, const TimeSignature& timeSignature, double intensity, const std::filesystem::path& filepath)
                :name(name), header(header), tempo(tempo), length(length), timeSignature(timeSignature), intensity(intensity), filepath(filepath)
                {}
        };

        struct ColumnId {
            enum Type {Name, Header, Tempo, Length, TimeSignature, Intensity};
        };

        void flush(void);
        void addBeat(const Beat &beat);

        void onSetupBeatLabel(const Glib::RefPtr<Gtk::ListItem>& list_item);
        void onSetupBeatProgressBar(const Glib::RefPtr<Gtk::ListItem>& list_item);
        void onBindBeat(const Glib::RefPtr<Gtk::ListItem>& list_item, ColumnId::Type id);
        Glib::RefPtr<Gdk::ContentProvider> onBeatDragPrepareData(const std::filesystem::path& filepath, double, double);

        void onActivate(guint pos);

    protected:
        Gtk::ColumnView columnView;
        Glib::RefPtr<Gio::ListStore<ModelColumns>> refListStore;
        Glib::RefPtr<Gtk::SingleSelection> selectionModel;
        const Library *library;
        Player *player;
};

#endif // BEAT_LIST_VIEW

/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include <gtkmm.h>

class BeatColumnView;
class LibraryListView;
class Player;
class PlayerFrame;

class MainWindow : public Gtk::ApplicationWindow {
    public:
        MainWindow();
        virtual ~MainWindow();

    protected:
        Gtk::Box box;
        Gtk::Grid grid;

        PlayerFrame *playerFrame;

        Gtk::Paned horizontalPaned;

        LibraryListView *libraryListView;
        BeatColumnView *beatColumnView;
        Player *player;

        Glib::RefPtr<Gtk::Builder> builder;
};

#endif // MAIN_WINDOW_HPP

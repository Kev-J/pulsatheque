/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "PlayerFrame.hpp"

#include "BeatColumnView.hpp"
#include "Player.hpp"

PlayerFrame::PlayerFrame()
:box(Gtk::Orientation::HORIZONTAL),
playButton("Play"),
stopButton("Stop"),
player(nullptr),
beatColumnView(nullptr)
{
    set_label("Player");
    box.set_margin(10);
    box.append(playButton);
    box.append(stopButton);
    this->set_child(box);

    playButton.signal_clicked().connect(sigc::bind(
            sigc::mem_fun(*this, &PlayerFrame::onButtonClicked), Button::Play));
    stopButton.signal_clicked().connect(sigc::bind(
            sigc::mem_fun(*this, &PlayerFrame::onButtonClicked), Button::Stop));
}

void PlayerFrame::setPlayer(Player *player)
{
    this->player = player;
}

void PlayerFrame::setBeatColumnView(BeatColumnView* beatColumnView)
{
    this->beatColumnView = beatColumnView;
}

void PlayerFrame::onButtonClicked(Button button)
{
    switch (button) {
        case Button::Play:
            if (beatColumnView != nullptr) {
               beatColumnView->playSelected(); 
            }
            break;
        case Button::Stop:
            if (player != nullptr) {
                player->stop();
            }
            break;
    }
}

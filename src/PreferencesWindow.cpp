/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "PreferencesWindow.hpp"

#include <iostream>

#include "Config.hpp"

PreferencesWindow::PreferencesWindow()
:mainBox(Gtk::Orientation::VERTICAL),
libraryPathBox(Gtk::Orientation::HORIZONTAL),
libraryPathButtonBox(Gtk::Orientation::VERTICAL),
libraryPathAddButton("Add"),
libraryPathRemoveButton("Remove")
{
    set_title("Preferences");
    set_default_size(300, 200);

    mainBox.set_margin(10);
    set_child(mainBox);

    notebook.set_margin(10);
    notebook.set_expand();
    mainBox.append(notebook);

    // Library paths
    libraryScrolledWindow.set_child(libraryListView);
    libraryScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
    libraryScrolledWindow.set_expand();
    libraryPathBox.append(libraryScrolledWindow);

    libraryPathStringList = Gtk::StringList::create({});
    libraryPathSelectionModel = Gtk::SingleSelection::create(libraryPathStringList);
    libraryPathSelectionModel->set_autoselect(false);
    libraryPathSelectionModel->set_can_unselect(true);
    libraryListView.set_model(libraryPathSelectionModel);
    libraryListView.add_css_class("data-table");

    auto factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::mem_fun(*this, &PreferencesWindow::onSetupLabel));
    factory->signal_bind().connect(sigc::mem_fun(*this, &PreferencesWindow::onBindName));
    libraryListView.set_factory(factory);

    libraryPathAddButton.signal_clicked().connect(sigc::mem_fun(*this, &PreferencesWindow::onLibraryPathAddButtonClicked));
    libraryPathButtonBox.append(libraryPathAddButton);

    libraryPathRemoveButton.signal_clicked().connect(sigc::mem_fun(*this, &PreferencesWindow::onLibraryPathRemoveButtonClicked));
    libraryPathButtonBox.append(libraryPathRemoveButton);

    libraryPathBox.append(libraryPathButtonBox);

    notebook.append_page(libraryPathBox, "Library paths");

    readPaths();
}

void PreferencesWindow::readPaths()
{

    while (libraryPathStringList->get_n_items() > 0) {
        libraryPathStringList->remove(0);
    }

    std::list<std::filesystem::path> paths;
    if (!Config::getLibraryPaths(paths)) {
        std::cerr << "Unable to read library paths" << std::endl;
        return;
    }

    for (const auto& path : paths) {
        libraryPathStringList->append(path.string());
    }
}

void PreferencesWindow::onSetupLabel(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
    list_item->set_child(*Gtk::make_managed<Gtk::Label>("", Gtk::Align::START));
}

void PreferencesWindow::onBindName(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
    auto pos = list_item->get_position();

    if (pos == GTK_INVALID_LIST_POSITION) {
        return;
    }

    auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
    if (!label) {
        return;
    }

    label->set_text(libraryPathStringList->get_string(pos));
}

void PreferencesWindow::onLibraryPathAddButtonClicked()
{
    auto dialog = new Gtk::FileChooserDialog("Add library path", Gtk::FileChooser::Action::SELECT_FOLDER);
    dialog->set_transient_for(*this);
    dialog->set_modal(true);
    dialog->signal_response().connect(sigc::bind(sigc::mem_fun(*this, &PreferencesWindow::onAddLibraryPathDialogResponse), dialog));

    dialog->add_button("_Cancel", Gtk::ResponseType::CANCEL);
    dialog->add_button("Add", Gtk::ResponseType::OK);

    dialog->show();
}


void PreferencesWindow::onAddLibraryPathDialogResponse(int response_id, Gtk::FileChooserDialog* dialog)
{
    if (response_id == Gtk::ResponseType::OK) {
        Config::addLibraryPath(dialog->get_file()->get_path());
        readPaths();
    }

    delete dialog;
}

void PreferencesWindow::onLibraryPathRemoveButtonClicked()
{
    auto sel = libraryPathSelectionModel->get_selected();
    if (sel != GTK_INVALID_LIST_POSITION) {
        Config::removeLibraryPath(sel);
        readPaths();
    }
}

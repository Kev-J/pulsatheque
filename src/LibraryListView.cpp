/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "LibraryListView.hpp"

#include <iostream>

#include "BeatColumnView.hpp"
#include "Config.hpp"
#include "Library.hpp"
#include "LibraryManager.hpp"

LibraryListView::LibraryListView()
:beatColumnView(nullptr)
{
    libManager = new LibraryManager();

    set_child(listView);

    refStringList = Gtk::StringList::create({});
    selectionModel = Gtk::SingleSelection::create(refStringList);
    listView.set_model(selectionModel);

    auto factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::mem_fun(*this, &LibraryListView::onSetupLibrary));
    factory->signal_bind().connect(sigc::mem_fun(*this, &LibraryListView::onBindLibrary));
    listView.set_factory(factory);

    selectionModel->property_selected().signal_changed().connect(sigc::mem_fun(*this, &LibraryListView::onSelectionChanged));
}

LibraryListView::~LibraryListView()
{
    delete libManager;
}

bool LibraryListView::refreshLibraries()
{
    flush();

    std::list<std::filesystem::path> libPath;
    if (!Config::getLibraryPaths(libPath)) {
        return false;
    }

    libManager->scan(libPath);

    for (const auto& lib : libManager->getLibraries()) {
        append(lib->getName());
    }

    return true;
}

void LibraryListView::flush()
{
    while (refStringList->get_n_items() > 0) {
        refStringList->remove(0);
    }
}

void LibraryListView::append(std::string libName)
{
    refStringList->append(libName);
}

void LibraryListView::setBeatColumnView(BeatColumnView* beatColumnView)
{
    this->beatColumnView = beatColumnView;
}

void LibraryListView::onSetupLibrary(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto label = Gtk::make_managed<Gtk::Label>();
  label->set_halign(Gtk::Align::START);
  list_item->set_child(*label);
}

void LibraryListView::onBindLibrary(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
  auto pos = list_item->get_position();
  if (pos == GTK_INVALID_LIST_POSITION)
    return;
  auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
  if (!label)
    return;
  label->set_text(refStringList->get_string(pos));
}

void LibraryListView::onSelectionChanged()
{
    auto sel = selectionModel->get_selected();

    auto i = libManager->getLibraries().begin();
    std::advance(i, sel);

    beatColumnView->setLibrary(*i);
}

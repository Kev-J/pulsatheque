/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "TTParser.hpp"

#include <functional>
#include <iostream>
#include <sqlite3.h>
#include <string>
#include <string.h>

#include "Beat.hpp"
#include "Library.hpp"

static int invoke_function(void* ptr, int argc, char **argv, char **colName) {
    return (*static_cast<std::function<int(int, char**, char**)>*>(ptr))(argc, argv, colName);
}

Library* TTParser::parse(std::filesystem::path path)
{
    Library *lib = new Library();
    lib->setPath(path);

    for (const auto& dir : std::filesystem::directory_iterator(path)) {
        std::filesystem::path dbFilePath = dir.path();
        dbFilePath += std::filesystem::path::preferred_separator;
        dbFilePath += "midiDB";
        if (!readFile(lib, dbFilePath)) {
            std::cerr << "Unable to read " + dbFilePath.string() << std::endl;
        }
    }

    if (lib->getAllBeats().size() <= 0) {
        delete lib;
        return nullptr;
    }

    return lib;
}

bool TTParser::readFile(Library* lib, std::filesystem::path filePath)
{
    sqlite3 *db;
    int ret;

    if (!std::filesystem::exists(filePath)) {
        return false;
    }

    ret = sqlite3_open(filePath.c_str(), &db);
    if (ret != SQLITE_OK) {
        std::cerr << "Cannot open database: " << filePath.c_str() << std::endl;
        std::cerr << sqlite3_errmsg(db) << std::endl;
        sqlite3_close(db);
        return false;
    }

    // Parse name
    if (!parseName(lib, db)) {
        std::cerr << "Unable to parse name" << std::endl;
        sqlite3_close(db);
        return false;
    }

    // Parse time signatures
    std::map<unsigned int, TimeSignature> tsMap;
    if (!parseTimeSignatures(tsMap, db)) {
        std::cerr << "Unable to parse time signatures" << std::endl;
        sqlite3_close(db);
        return false;
    }

    // Parse filenames
    std::map<unsigned int, std::string> filenameMap;
    if (!parseFilenames(filenameMap, db)) {
        std::cerr << "Unable to parse filenames" << std::endl;
        sqlite3_close(db);
        return false;
    }

    // Parse paths
    std::map<unsigned int, std::string> pathMap;
    if (!parsePaths(pathMap, db)) {
        std::cerr << "Unable to parse paths" << std::endl;
        sqlite3_close(db);
        return false;
    }

    // Parse headers
    std::map<unsigned int, std::string> headerMap;
    if (!parseHeaders(headerMap, db)) {
        std::cerr << "Unable to parse headers" << std::endl;
        sqlite3_close(db);
        return false;
    }

    // Parse beats
    if (!parseBeats(lib, db, tsMap, filenameMap, pathMap, headerMap)) {
        std::cerr << "Unable to parse beats" << std::endl;
        sqlite3_close(db);
        return false;
    }

    // TODO get data

    /*
    select * FROM MIDIENTITY;
21784|fill 04|4|3103|39|603|6|100|23.6867|19.739|3|8||0|0|1|5
.tables
FILENAMES         MIDINOW_MAT       TIMESIGS          rel_fts_content 
GENRE             PATHS             ent_fts           rel_fts_docsize 
HEADER            RANGES            ent_fts_content   rel_fts_segdir  
KITCOUNT          REL_ENT_KITS      ent_fts_docsize   rel_fts_segments
KITPIECES         REL_ENT_TAGS      ent_fts_segdir    rel_fts_stat    
LIBRARY           RESOLUTION        ent_fts_segments
LIBRARY_PARENTS   TAGGROUP          ent_fts_stat    
MIDIENTITY        TAGS              rel_fts

    select * from LIBRARY;
39|000343@METAL_MACHINE|Metal Machine|1.2.0|000343@METAL_MACHINE

select * from FILENAMES;
1|Variation_01.mid
2|Variation_02.mid
3|Variation_03.mid
4|Variation_04.mid
5|Variation_05.mid
6|Variation_06.mid
7|Variation_07.mid
8|Variation_08.mid
9|Variation_09.mid
10|Variation_10.mid
11|Variation_11.mid
12|Variation_12.mid
13|Variation_13.mid
14|Variation_14.mid
15|Variation_15.mid

select * from TAGS;
1|Beat|1|1
2|Fill|1|1
3|Ending|1|1
4|Straight|1|1
5|Swing|1|1
6|Half Time|1|1
7|Normal Time|1|1
8|Double Time|1|1
11|Hard Hits|1|1
12|Shuffle|2|1
13|Standard|2|1
14|Double Kick|2|1
15|March|2|1
19|Two Beat|2|1
21|Jam Track|1|1
*/
    sqlite3_close(db);

    return true;
}

bool TTParser::parseName(Library *lib, sqlite3 *db)
{
    char *errMsg = nullptr;

    int ret = sqlite3_exec(db,
        "SELECT * FROM LIBRARY",
        &invoke_function,
        new std::function<int(int, char**, char**)>([lib](int argc, char **argv, char **colName) {
            for (int i = 0 ; i < argc ; i++) {
                if (!strcmp(colName[i], "FormattedName")) {
                    lib->setName(std::string(argv[i]));
                } else if (!strcmp(colName[i], "Version")) {
                    lib->setVersion(std::string(argv[i]));
                }
            }

            return 0;
        }),
    &errMsg);

    if (ret != SQLITE_OK) {
        std::cerr << "Cannot select" << std::endl;
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
        return false;
    }

    return true;
}

bool TTParser::parseTimeSignatures(std::map<unsigned int, TimeSignature>& tsMap, sqlite3* db)
{
    char *errMsg = nullptr;

    int ret = sqlite3_exec(db,
        "SELECT * FROM TIMESIGS",
        &invoke_function,
        new std::function<int(int, char**, char**)>([&tsMap](int argc, char **argv, char **colName) {
            unsigned int id;
            std::string ts = std::string("0/0");
            for (int i = 0 ; i < argc ; i++) {
                if (!strcmp(colName[i], "ID")) {
                    id = std::stoi(argv[i]);
                } else if (!strcmp(colName[i], "Name")) {
                    ts = std::string(argv[i]);
                }
            }

            size_t slashPos = ts.find('/');
            if (slashPos == std::string::npos) {
                return -1;
            }

            unsigned int num = std::stoi(ts.substr(0, slashPos));
            unsigned int den = std::stoi(ts.substr(slashPos + 1, ts.length() - slashPos));
            tsMap.insert(std::pair<unsigned int, TimeSignature>(id, TimeSignature(num, den)));
            return 0;
        }),
    &errMsg);

    if (ret != SQLITE_OK) {
        std::cerr << "Cannot select" << std::endl;
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
        return false;
    }

    return true;
}

bool TTParser::parseFilenames(std::map<unsigned int, std::string>& filenameMap, sqlite3* db)
{
    char *errMsg = nullptr;

    int ret = sqlite3_exec(db,
        "SELECT * FROM FILENAMES",
        &invoke_function,
        new std::function<int(int, char**, char**)>([&filenameMap](int argc, char **argv, char **colName) {
            unsigned int id;
            std::string filename = std::string("<empty>");
            for (int i = 0 ; i < argc ; i++) {
                if (!strcmp(colName[i], "ID")) {
                    id = std::stoi(argv[i]);
                } else if (!strcmp(colName[i], "Name")) {
                    filename = std::string(argv[i]);
                }
            }

            filenameMap[id] = filename;
            return 0;
        }),
    &errMsg);

    if (ret != SQLITE_OK) {
        std::cerr << "Cannot select" << std::endl;
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
        return false;
    }

    return true;
}

bool TTParser::parsePaths(std::map<unsigned int, std::string>& pathMap, sqlite3* db)
{
    char *errMsg = nullptr;

    int ret = sqlite3_exec(db,
        "SELECT * FROM PATHS",
        &invoke_function,
        new std::function<int(int, char**, char**)>([&pathMap](int argc, char **argv, char **colName) {
            unsigned int id;
            std::string path = std::string("");
            for (int i = 0 ; i < argc ; i++) {
                if (!strcmp(colName[i], "ID")) {
                    id = std::stoi(argv[i]);
                } else if (!strcmp(colName[i], "Path")) {
                    path = std::string(argv[i]);
                }
            }

            pathMap[id] = path;
            return 0;
        }),
    &errMsg);

    if (ret != SQLITE_OK) {
        std::cerr << "Cannot select" << std::endl;
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
        return false;
    }

    return true;
}

bool TTParser::parseHeaders(std::map<unsigned int, std::string>& headerMap, sqlite3* db)
{
    char *errMsg = nullptr;

    int ret = sqlite3_exec(db,
        "SELECT * FROM HEADER",
        &invoke_function,
        new std::function<int(int, char**, char**)>([&headerMap](int argc, char **argv, char **colName) {
            unsigned int id;
            std::string header = std::string("<empty>");
            for (int i = 0 ; i < argc ; i++) {
                if (!strcmp(colName[i], "ID")) {
                    id = std::stoi(argv[i]);
                } else if (!strcmp(colName[i], "Name")) {
                    header = std::string(argv[i]);
                }
            }

            headerMap[id] = header;

            return 0;
        }),
    &errMsg);

    if (ret != SQLITE_OK) {
        std::cerr << "Cannot select" << std::endl;
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
        return false;
    }

    return true;
}



bool TTParser::parseBeats(Library *lib,
                          sqlite3 *db,
                          std::map<unsigned int, TimeSignature>& tsMap,
                          std::map<unsigned int, std::string>& filenameMap,
                          std::map<unsigned int, std::string>& pathMap,
                          std::map<unsigned int, std::string>& headerMap)
{
    char *errMsg = nullptr;

    std::string filename;
    std::string path;

    int ret = sqlite3_exec(db,
        "SELECT * FROM MIDIENTITY",
        &invoke_function,
        new std::function<int(int, char**, char**)>([lib, &tsMap, &filenameMap, &pathMap, &headerMap, &filename, &path](int argc, char **argv, char **colName) {
            Beat *b = lib->createBeat();
            for (int i = 0 ; i < argc ; i++) {
                if (!strcmp(colName[i], "Name")) {
                    b->setName(std::string(argv[i]));
                } else if (!strcmp(colName[i], "Tempo")) {
                    b->setTempo(std::stod(argv[i]));
                } else if (!strcmp(colName[i], "Length")) {
                    b->setLength(std::stoi(argv[i]));
                } else if (!strcmp(colName[i], "Intensity")) {
                    b->setIntensity(std::stod(argv[i]) / 100.0);
                } else if (!strcmp(colName[i], "TimeSigID")) {
                    b->setTimeSignature(tsMap[std::stoi(argv[i])]);
                } else if (!strcmp(colName[i], "FilenameID")) {
                    filename = filenameMap[std::stoi(argv[i])];
                } else if (!strcmp(colName[i], "PathID")) {
                    path = pathMap[std::stoi(argv[i])];
                } else if (!strcmp(colName[i], "HeaderID")) {
                    b->setHeader(headerMap[std::stoi(argv[i])]);
                }
            }

            std::filesystem::path libPath = lib->getPath().string();
            libPath += std::filesystem::path::preferred_separator;
            size_t pos = path.find(TTParser::ROOT_PATH_IDENTIFIER);
            if (pos == std::string::npos) {
                std::cerr << "Path does not contains " << TTParser::ROOT_PATH_IDENTIFIER << std::endl;
                return -1;
            }
            path.replace(pos, TTParser::ROOT_PATH_IDENTIFIER.length(), libPath.string());
            std::filesystem::path filePath(path);
            filePath += std::filesystem::path::preferred_separator;
            filePath += filename;
            b->setFilePath(filePath);

            return 0;
        }),
    &errMsg);
    if (ret != SQLITE_OK) {
        std::cerr << "Cannot select" << std::endl;
        std::cerr << "SQL error: " << errMsg << std::endl;
        sqlite3_free(errMsg);
        return false;
    }

    return true;
}

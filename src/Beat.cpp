/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "Beat.hpp"

Beat::Beat()
:name("<unnamed>"), timeSignature(0, 0)
{
}

std::string Beat::getName() const
{
    return this->name;
}

double Beat::getTempo() const
{
    return this->tempo;
}

unsigned int Beat::getLength() const
{
    return this->length;
}

double Beat::getIntensity() const
{
    return this->intensity;
}

const TimeSignature& Beat::getTimeSignature(void) const
{
    return this->timeSignature;
}

const std::filesystem::path& Beat::getFilePath(void) const
{
    return this->filePath;
}

std::string Beat::getHeader() const
{
    return this->header;
}

void Beat::setName(const std::string& name)
{
    this->name = name;
}

void Beat::setTempo(double tempo)
{
    this->tempo = tempo;
}

void Beat::setLength(unsigned int length)
{
    this->length = length;
}

void Beat::setIntensity(double intensity)
{
    this->intensity = intensity;
}

void Beat::setTimeSignature(const TimeSignature& ts)
{
    this->timeSignature = ts;
}

void Beat::setFilePath(const std::filesystem::path& filePath)
{
    this->filePath = filePath;
}

void Beat::setHeader(const std::string& header)
{
    this->header = header;
}

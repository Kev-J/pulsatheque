/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#ifndef LIBRARY_HPP
#define LIBRARY_HPP

#include <filesystem>
#include <list>
#include <string>

class Beat;

class Library {
    public:
        Library();
        virtual ~Library();
        std::string getName(void) const;
        std::string getVersion(void) const;
        const std::filesystem::path& getPath(void);

        void setName(std::string name);
        void setVersion(std::string version);
        void setPath(const std::filesystem::path& path);

        Beat* createBeat(void);
        const std::list<Beat*>& getAllBeats(void) const;

        void flushBeats(void);

    private:
        std::string name;
        std::string version;
        std::list<Beat*> beats;
        std::filesystem::path path;
};

#endif // LIBRARY_HPP

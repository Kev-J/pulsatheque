/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "Library.hpp"

#include "Beat.hpp"

Library::Library()
:name("<unnamed>")
{
}

Library::~Library()
{
    flushBeats();
}

std::string Library::getName() const
{
    return this->name;
}

std::string Library::getVersion() const
{
    return this->version;
}

const std::filesystem::path& Library::getPath(void)
{
    return this->path;
}

void Library::setName(std::string name)
{
    this->name = name;
}

void Library::setVersion(std::string version)
{
    this->version = version;
}

void Library::setPath(const std::filesystem::path& path)
{
    this->path = path;
}

Beat* Library::createBeat()
{
    Beat* b = new Beat();

    this->beats.push_back(b);

    return b;
}

const std::list<Beat*>& Library::getAllBeats(void) const
{
    return this->beats;
}

void Library::flushBeats()
{
    for (const auto& b : this->beats) {
        delete b;
    }

    this->beats.clear();
}

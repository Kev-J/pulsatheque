/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#ifndef LIBRARY_LIST_VIEW
#define LIBRARY_LIST_VIEW

#include <string>

#include <gtkmm.h>

class BeatColumnView;
class LibraryManager;

class LibraryListView : public Gtk::ScrolledWindow {
    public:
        LibraryListView();
        virtual ~LibraryListView();

        void setBeatColumnView(BeatColumnView* beatColumnView);

        bool refreshLibraries(void);

    protected:
        void flush(void);
        void append(std::string libName);

        void onSetupLibrary(const Glib::RefPtr<Gtk::ListItem>& list_item);
        void onBindLibrary(const Glib::RefPtr<Gtk::ListItem>& list_item);
        void onSelectionChanged(void);

    protected:

        LibraryManager *libManager;
        Glib::RefPtr<Gtk::StringList> refStringList;
        Glib::RefPtr<Gtk::SingleSelection> selectionModel;
        Gtk::ListView listView;
        BeatColumnView* beatColumnView;
};

#endif // LIBRARY_LIST_VIEW

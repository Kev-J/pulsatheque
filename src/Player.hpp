/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <filesystem>
#include <atomic>
#include <list>
#include <vector>

#include <jack/jack.h>
class Beat;

class Player {
    public:
        Player();
        bool initJack(void);
        void terminateJack(void);
        bool play(const Beat& beat);
        void stop();

    protected:
        struct Event {
            int tick;
            std::vector<unsigned char> bytes;
        };

    protected:
        int jackCallback(jack_nframes_t cnt);

    protected:
        jack_client_t *jackClient;
        jack_port_t *jackPort;
        std::atomic_bool playing;
        jack_time_t lastUsecs;
        std::vector<Event> events;
        std::vector<Event>::iterator eventIdx;
        std::atomic<double> tempo;
        std::atomic<double> ticksPerBeat;
        std::atomic<double> duration;
        std::atomic<double> beatDuration;
        std::atomic<double> tickDuration;
        std::atomic<jack_time_t> startUs;
        std::mutex eventsMutex;
};

#endif // PLAYER_HPP

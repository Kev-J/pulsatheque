/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "PulsathequeApplication.hpp"

#include <iostream>

#include "MainWindow.hpp"
#include "PreferencesWindow.hpp"

PulsathequeApplication::PulsathequeApplication()
:Gtk::Application("org.kejo.pulsatheque")
{
    Glib::set_application_name("Pulsatheque");
}

Glib::RefPtr<PulsathequeApplication> PulsathequeApplication::create()
{
    return Glib::make_refptr_for_instance<PulsathequeApplication>(new PulsathequeApplication());
}

void PulsathequeApplication::on_startup()
{
    Gtk::Application::on_startup();

    add_action("preferences", sigc::mem_fun(*this, &PulsathequeApplication::onShowPreferences));

    builder = Gtk::Builder::create();
    Glib::ustring ui_info =
    "<interface>"
    "  <!-- menubar -->"
    "  <menu id='menu-example'>"
    "    <submenu>"
    "      <attribute name='label' translatable='yes'>_File</attribute>"
    "      <section>"
    "        <item>"
    "          <attribute name='label' translatable='yes'>_Preferences</attribute>"
    "          <attribute name='action'>app.preferences</attribute>"
    "          <attribute name='accel'>&lt;Primary&gt;p</attribute>"
    "        </item>"
    "      </section>"
    "    </submenu>"
    "  </menu>"
    "</interface>";
    try {
        builder->add_from_string(ui_info);
    } catch (const Glib::Error& ex) {
        std::cerr << "Building toolbar failed: " <<  ex.what();
    }

    auto object = builder->get_object("menu-example");
    auto gmenu = std::dynamic_pointer_cast<Gio::Menu>(object);
    if (!gmenu) {
      g_warning("GMenu not found");
    } else {
      set_menubar(gmenu);
    }
}

void PulsathequeApplication::on_activate()
{
    auto win = new MainWindow();

    add_window(*win);

    win->signal_hide().connect(sigc::bind(sigc::mem_fun(*this, &PulsathequeApplication::on_window_hide), win));

    win->set_show_menubar();
    win->show();
}

void PulsathequeApplication::onShowPreferences()
{
    auto win = new PreferencesWindow();

    add_window(*win);

    win->signal_hide().connect(sigc::bind(sigc::mem_fun(*this, &PulsathequeApplication::on_window_hide), win));

    win->set_modal();
    win->show();
}

void PulsathequeApplication::on_window_hide(Gtk::Window* window)
{
    delete window;
}

/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "MainWindow.hpp"

#include "BeatColumnView.hpp"
#include "LibraryListView.hpp"
#include "Player.hpp"
#include "PlayerFrame.hpp"

#include <filesystem>

MainWindow::MainWindow()
:box(Gtk::Orientation::VERTICAL),
horizontalPaned(Gtk::Orientation::HORIZONTAL)
{
    set_title("Pulsatheque");
    set_default_size(1024, 720);

    grid.set_margin(10);

    playerFrame = new PlayerFrame();
    grid.attach(*playerFrame, 0, 0);

    box.append(grid);
    box.set_vexpand(true);

    horizontalPaned.set_margin(10);
    horizontalPaned.set_vexpand(true);
    horizontalPaned.set_position(200);
    box.append(horizontalPaned);
    set_child(box);

    libraryListView = new LibraryListView();
    beatColumnView = new BeatColumnView();

    libraryListView->setBeatColumnView(beatColumnView);

    horizontalPaned.set_start_child(*libraryListView);
    horizontalPaned.set_end_child(*beatColumnView);

    libraryListView->refreshLibraries();

    player = new Player();
    beatColumnView->setPlayer(player);
    playerFrame->setPlayer(player);
    playerFrame->setBeatColumnView(beatColumnView);

    if (!player->initJack()) {
        std::cerr << "Unable to init jack" << std::endl;
    }
}

MainWindow::~MainWindow()
{
    libraryListView->setBeatColumnView(nullptr);
    delete libraryListView;

    delete beatColumnView;

    delete playerFrame;

    player->terminateJack();
    delete player;
}

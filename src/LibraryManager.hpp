/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#ifndef LIBRARY_MANAGER_HPP
#define LIBRARY_MANAGER_HPP

#include <filesystem>
#include <list>

class Library;

class LibraryManager {
    public:
        LibraryManager(void);
        virtual ~LibraryManager(void);

        void flush(void);
        void scan(std::list<std::filesystem::path> &paths);
        const std::list<Library*>& getLibraries(void) const;

    private:
        std::list<Library*> libraries;
};
#endif // LIBRARY_MANAGER_HPP

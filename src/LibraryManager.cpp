/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "LibraryManager.hpp"

#include "Library.hpp"
#include "TTParser.hpp"

LibraryManager::LibraryManager()
{
}

LibraryManager::~LibraryManager()
{
    flush();
}

void LibraryManager::flush()
{
    for (const auto& lib : libraries) {
        delete lib;
    }

    libraries.clear();
}

void LibraryManager::scan(std::list<std::filesystem::path> &paths)
{
    for (const auto& path : paths) {
        TTParser parser;

        Library *lib = parser.parse(path);
        if (lib != nullptr) {
            this->libraries.push_back(lib);
        }
    }
}

const std::list<Library*>& LibraryManager::getLibraries() const
{
    return this->libraries;
}

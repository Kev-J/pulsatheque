/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "Config.hpp"

#include <iostream>
#include <string>

#include <libconfig.h++>

namespace Config {
    static const std::string filename = "pulsatheque.cfg";
    const std::filesystem::path getConfigDirPath(void)
    {
#ifdef __linux__
        std::filesystem::path p(std::getenv("HOME"));
        p += "/.config/pulsatheque/";
#elif _WIN32
#error "Not supported"
#else
#error "Not supported"
#endif
        return p;
    }

    const std::filesystem::path getConfigFilePath(void)
    {
        std::filesystem::path p = getConfigDirPath();
        p += filename;
        return p;
    }
}

bool Config::getLibraryPaths(std::list<std::filesystem::path> &paths)
{
    libconfig::Config cfg;

    try {
        cfg.readFile(getConfigFilePath().c_str());
    } catch (const libconfig::FileIOException &fioex) {
        std::cerr << "Couldn't read " << getConfigFilePath() << std::endl;
        return false;
    } catch (const libconfig::ParseException &pex) {
        std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine() << ": " << pex.getError() << std::endl;
        return false;
    }

    const libconfig::Setting& root = cfg.getRoot();

    try {
        const libconfig::Setting &libraryPaths = root["library_path"];

        int count = libraryPaths.getLength();
        for (int i = 0 ; i < count ; i++) {
            paths.push_back(libraryPaths[i].c_str());
        }

    } catch (const libconfig::SettingNotFoundException &nfex) {
        std::cerr << "library_path setting not found" << std::endl;
        return false;
    }

    return true;
}

bool Config::addLibraryPath(const std::filesystem::path& path)
{
    libconfig::Config cfg;

#ifdef __linux__
    if (std::filesystem::exists(getConfigFilePath())) {
        try {
            cfg.readFile(getConfigFilePath().c_str());
        } catch (const libconfig::FileIOException &fioex) {
            std::cerr << "Couldn't read " << getConfigFilePath() << std::endl;
            return false;
        } catch (const libconfig::ParseException &pex) {
            std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine() << ": " << pex.getError() << std::endl;
            return false;
        }
    } else {
        if (!std::filesystem::create_directories(getConfigDirPath())) {
            std::cerr << "Unable to create directory " << getConfigDirPath() << std::endl;
            return false;
        }
    }
#else
#error "Not supported"
#endif

    libconfig::Setting &root = cfg.getRoot();
    if (!cfg.exists("library_path")) {
        root.add("library_path", libconfig::Setting::TypeList);
    }

    try {
        libconfig::Setting &libraryPaths = root["library_path"];
        libraryPaths.add(libconfig::Setting::TypeString) = path;
    } catch (const libconfig::SettingNotFoundException &nfex) {
        std::cerr << "library_path setting not found" << std::endl;
        return false;
    }

    try {
        cfg.writeFile(getConfigFilePath().c_str());
    } catch (const libconfig::FileIOException &fioex) {
        std::cerr << "Couln't write to " << getConfigFilePath() << std::endl;
        return false;
    }

    return true;
}

bool Config::removeLibraryPath(unsigned int id)
{
    libconfig::Config cfg;

    try {
        cfg.readFile(getConfigFilePath().c_str());
    } catch (const libconfig::FileIOException &fioex) {
        std::cerr << "Couldn't read " << getConfigFilePath() << std::endl;
        return false;
    } catch (const libconfig::ParseException &pex) {
        std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine() << ": " << pex.getError() << std::endl;
        return false;
    }

    const libconfig::Setting& root = cfg.getRoot();

    try {
        libconfig::Setting &libraryPaths = root["library_path"];
        libraryPaths.remove(id);
    } catch (const libconfig::SettingNotFoundException &nfex) {
        std::cerr << "library_path setting not found" << std::endl;
        return false;
    }

    try {
        cfg.writeFile(getConfigFilePath().c_str());
    } catch (const libconfig::FileIOException &fioex) {
        std::cerr << "Couln't write to " << getConfigFilePath() << std::endl;
        return false;
    }
    return true;
}

/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#ifndef PULSATHEQUE_APPLICATION_HPP
#define PULSATHEQUE_APPLICATION_HPP

#include <gtkmm.h>

class PulsathequeApplication: public Gtk::Application {
public:
    static Glib::RefPtr<PulsathequeApplication> create(void);

protected:
    PulsathequeApplication();

    void on_startup(void) override;
    void on_activate(void) override;

    void onShowPreferences(void);

private:
    void on_window_hide(Gtk::Window* window);

    Glib::RefPtr<Gtk::Builder> builder;
};

#endif //PULSATHEQUE_APPLICATION_HPP

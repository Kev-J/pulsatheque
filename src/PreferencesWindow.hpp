/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#ifndef PREFERENCES_WINDOW
#define PREFERENCES_WINDOW

#include <gtkmm.h>

class PreferencesWindow: public Gtk::Window {
public:
    PreferencesWindow();

protected:
    void readPaths(void);

    void onSetupLabel(const Glib::RefPtr<Gtk::ListItem>& list_item);
    void onBindName(const Glib::RefPtr<Gtk::ListItem>& list_item);

    void onLibraryPathAddButtonClicked(void);
    void onAddLibraryPathDialogResponse(int response_id, Gtk::FileChooserDialog* dialog);
    void onLibraryPathRemoveButtonClicked(void);

protected:
    Gtk::Box mainBox;
    Gtk::Notebook notebook;

    Gtk::Box libraryPathBox;
    Gtk::Box libraryPathButtonBox;
    Gtk::ScrolledWindow libraryScrolledWindow;
    Gtk::ListView libraryListView;
    Glib::RefPtr<Gtk::StringList> libraryPathStringList;
    Glib::RefPtr<Gtk::SingleSelection> libraryPathSelectionModel;
    Gtk::Button libraryPathAddButton;
    Gtk::Button libraryPathRemoveButton;
};

#endif // PREFERENCES_WINDOW

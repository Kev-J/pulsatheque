/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include <iostream>
#include <getopt.h>

#include "PulsathequeApplication.hpp"

void printUsage(void)
{
    std::cout << "pulsatheque [OPTIONS]" << std::endl;
    std::cout << std::endl;
    std::cout << "-h         Display this help" << std::endl;
    std::cout << "-v         Display version" << std::endl;
}

void printVersion(void)
{
    std::cout << "Pulsatheque version 0.1" << std::endl;
    std::cout << "Copyright (C) Kevin Joly" << std::endl;
    std::cout << "Licensed under GPLv3" << std::endl;
}

int main(int argc, char* argv[])
{
    switch(getopt(argc, argv, "hv")) {
        case 'h':
            printUsage();
            break;
        case 'v':
            printVersion();
            break;
    }

    auto app = PulsathequeApplication::create();

    return app->run(argc, argv);
}

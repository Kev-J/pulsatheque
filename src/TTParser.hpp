/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#ifndef TT_PARSER_HPP
#define TT_PARSER_HPP

#include <map>

#include "Parser.hpp"
#include "TimeSignature.hpp"

struct sqlite3;

class TTParser: public Parser {
    public:
        virtual Library* parse(std::filesystem::path path);

    private:
        bool readFile(Library *lib, std::filesystem::path filepath);
        bool parseName(Library *lib, sqlite3 *db);
        bool parseTimeSignatures(std::map<unsigned int, TimeSignature>& tsMap, sqlite3* db);
        bool parseFilenames(std::map<unsigned int, std::string>& filenameMap, sqlite3* db);
        bool parsePaths(std::map<unsigned int, std::string>& filenameMap, sqlite3* db);
        bool parseHeaders(std::map<unsigned int, std::string>& headerMap, sqlite3* db);
        bool parseBeats(Library *lib,
                        sqlite3 *db,
                        std::map<unsigned int, TimeSignature>& tsMap,
                        std::map<unsigned int, std::string>& filenameMap,
                        std::map<unsigned int, std::string>& headerMap,
                        std::map<unsigned int, std::string>& pathMap);
    private:
        static constexpr std::string_view ROOT_PATH_IDENTIFIER = "_EZD2MIDI_";
};

#endif // TT_PARSER_HPP

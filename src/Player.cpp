/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "Player.hpp"

#include <libremidi/reader.hpp>

#include <jack/midiport.h>

#include <fstream>
#include <functional>
#include <iostream>

#include <math.h>
#include <cstring>

#include "Beat.hpp"

static int invoke_function(jack_nframes_t cnt, void* ctx) {
    return (*static_cast<std::function<int(jack_nframes_t)>*>(ctx))(cnt);
}

Player::Player()
:jackClient(nullptr), jackPort(nullptr), playing(false)
{
}

bool Player::initJack()
{
    lastUsecs = 0;
    jack_status_t status{};
    jackClient = jack_client_open("Pulsatheque", JackNoStartServer, &status);

    if (!jackClient) {
        jackClient = nullptr;
        return false;
    }

    if (!jack_is_realtime(jackClient)) {
        std::cerr << "Warning: Jack is not realtime" << std::endl;
    }

    //if (mlockall (MCL_CURRENT | MCL_FUTURE)) {
    //    std::cerr << "Warning: Can not lock memory." << std::endl;
    //}

    jackPort = jack_port_register(jackClient, "Output", JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
    if (jackPort == NULL) {
        jackPort = nullptr;
        jack_client_close(jackClient);
        return false;
    }

    jack_set_process_callback(jackClient, &invoke_function, new std::function<int(jack_nframes_t)>([this](jack_nframes_t cnt) {
        return this->jackCallback(cnt);
        }));

    if (jack_activate(jackClient)) {
        jack_client_close(jackClient);
        jackClient = nullptr;
        return false;
    }

    return true;
}

void Player::terminateJack()
{
    stop();

    if (jackPort) {
        jack_port_unregister(jackClient, jackPort);
        jackPort = nullptr;
    }

    if (jackClient) {
        jack_client_close(jackClient);
        jackClient = nullptr;
    }
}

bool Player::play(const Beat& beat)
{
    stop();

    std::ifstream file{beat.getFilePath(), std::ios::binary};
    if (!file.is_open()) {
        std::cerr << "Unable to open " << beat.getFilePath() << std::endl;
        return false;
    }

    if (!jackClient) {
        std::cerr << "Jack is not runing" << std::endl;
        return false;
    }

    std::vector<uint8_t> data;
    data.assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());

    libremidi::reader r{true};
    libremidi::reader::parse_result result = r.parse(data);

    if (result != libremidi::reader::invalid) {
        eventsMutex.lock();

        tempo = beat.getTempo();
        beatDuration = 60. * 1000000. / tempo;
        ticksPerBeat = r.ticksPerBeat;
        tickDuration = beatDuration / ticksPerBeat;
        duration = r.get_end_time() * tickDuration;

        events.clear();
        for (const auto& track : r.tracks) {
            for (const libremidi::track_event& event : track) {
                Event e;
                e.tick = event.tick;
                for (const auto &ev : event.m.bytes) {
                    e.bytes.push_back(ev);
                }
                events.push_back(e);
            }
        }
        eventIdx = events.begin();
        startUs = jack_get_time();
        eventsMutex.unlock();
    }

    playing = true;
    return true;
}

void Player::stop()
{
    playing = false;
}

int Player::jackCallback(jack_nframes_t cnt)
{
    if (!playing) {
        return 0;
    }

    if (!eventsMutex.try_lock()) {
        return 0;
    }

    void* buf = jack_port_get_buffer(jackPort, cnt);

    jack_midi_clear_buffer(buf);

    jack_time_t usecs = jack_get_time();
    if (usecs < startUs) {
        eventsMutex.unlock();
        return 0;
    }

    long tick = llrint((usecs - startUs) / tickDuration);
    while (tick >= (*eventIdx).tick) {
        uint8_t *buffer = jack_midi_event_reserve(buf, 0, (*eventIdx).bytes.size());
        if (buffer != NULL) {
            memcpy(buffer, (*eventIdx).bytes.data(), (*eventIdx).bytes.size());
        }

        if (eventIdx == std::prev(events.end())) {
            eventIdx = events.begin();
            startUs += duration;
            if (usecs >= startUs) {
                tick = llrint((usecs - startUs) / tickDuration);
            } else {
                break;
            }
        } else {
            eventIdx++;
        }
    }

    eventsMutex.unlock();

    return 0;
}

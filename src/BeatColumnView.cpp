/*
 * Author: Kevin Joly <kevin.joly@posteo.net>
 * Copyright (c) 2024
 *
 * This file is part of Pulsathèque.
 *
 * Pulsathèque is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Pulsathèque is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not, see <https://www.gnu.org/licenses/>. 4
 */
#include "BeatColumnView.hpp"

#include "Beat.hpp"
#include "Library.hpp"
#include "Player.hpp"

BeatColumnView::BeatColumnView()
:library(nullptr), player(nullptr)
{
    refListStore = Gio::ListStore<ModelColumns>::create();
    selectionModel = Gtk::SingleSelection::create(refListStore);
    columnView.set_model(selectionModel);

    set_child(columnView);

    // Name
    auto factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::mem_fun(*this, &BeatColumnView::onSetupBeatLabel));
    factory->signal_bind().connect(sigc::bind(sigc::mem_fun(*this, &BeatColumnView::onBindBeat), ColumnId::Name));

    auto col = Gtk::ColumnViewColumn::create("Name", factory);
    columnView.append_column(col);

    // Header
    factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::mem_fun(*this, &BeatColumnView::onSetupBeatLabel));
    factory->signal_bind().connect(sigc::bind(sigc::mem_fun(*this, &BeatColumnView::onBindBeat), ColumnId::Header));

    col = Gtk::ColumnViewColumn::create("Header", factory);
    columnView.append_column(col);

    // Tempo
    factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::mem_fun(*this, &BeatColumnView::onSetupBeatLabel));
    factory->signal_bind().connect(sigc::bind(sigc::mem_fun(*this, &BeatColumnView::onBindBeat), ColumnId::Tempo));

    col = Gtk::ColumnViewColumn::create("Tempo", factory);
    columnView.append_column(col);

    // Length
    factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::mem_fun(*this, &BeatColumnView::onSetupBeatLabel));
    factory->signal_bind().connect(sigc::bind(sigc::mem_fun(*this, &BeatColumnView::onBindBeat), ColumnId::Length));

    col = Gtk::ColumnViewColumn::create("Length", factory);
    columnView.append_column(col);

    // Time signature
    factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::mem_fun(*this, &BeatColumnView::onSetupBeatLabel));
    factory->signal_bind().connect(sigc::bind(sigc::mem_fun(*this, &BeatColumnView::onBindBeat), ColumnId::TimeSignature));

    col = Gtk::ColumnViewColumn::create("Time signature", factory);
    columnView.append_column(col);

    // Intensity
    factory = Gtk::SignalListItemFactory::create();
    factory->signal_setup().connect(sigc::mem_fun(*this, &BeatColumnView::onSetupBeatProgressBar));
    factory->signal_bind().connect(sigc::bind(sigc::mem_fun(*this, &BeatColumnView::onBindBeat), ColumnId::Intensity));

    col = Gtk::ColumnViewColumn::create("Intensity", factory);
    columnView.append_column(col);

    columnView.signal_activate().connect(sigc::mem_fun(*this, &BeatColumnView::onActivate));
}

BeatColumnView::~BeatColumnView()
{
}

void BeatColumnView::setLibrary(const Library *library)
{
    this->library = library;

    flush();

    for (const auto b : library->getAllBeats()) {
        addBeat(*b);
    }
}

void BeatColumnView::setPlayer(Player *player)
{
    this->player = player;
}

bool BeatColumnView::playSelected(void)
{
    if (player != nullptr) {
        auto pos = selectionModel->get_selected();
        auto i = library->getAllBeats().begin();
        std::advance(i, pos);
        return player->play(**i);
    } else {
        return false;
    }
}

void BeatColumnView::flush()
{
    while (refListStore->get_n_items() > 0) {
        refListStore->remove(0);
    }
}

void BeatColumnView::addBeat(const Beat &beat)
{
    refListStore->append(ModelColumns::create(beat.getName(),
                                              beat.getHeader(),
                                              beat.getTempo(),
                                              beat.getLength(),
                                              beat.getTimeSignature(),
                                              beat.getIntensity(),
                                              beat.getFilePath()));
}

void BeatColumnView::onSetupBeatLabel(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
    list_item->set_child(*Gtk::make_managed<Gtk::Label>());
}

void BeatColumnView::onSetupBeatProgressBar(const Glib::RefPtr<Gtk::ListItem>& list_item)
{
    auto pb = Gtk::make_managed<Gtk::ProgressBar>();
    list_item->set_child(*pb);
}

void BeatColumnView::onBindBeat(const Glib::RefPtr<Gtk::ListItem>& list_item, ColumnId::Type id)
{
    auto item = list_item->get_item();
    if (auto m = std::dynamic_pointer_cast<ModelColumns>(item)) {
        if (id == ColumnId::Name) {
            auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
            if (label) {
                label->set_text(m->name);
            }
        } else if (id == ColumnId::Tempo) {
            auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
            if (label) {
                std::ostringstream tempoStr;
                tempoStr << std::setprecision(8);
                tempoStr << std::noshowpoint;
                tempoStr << m->tempo;
                label->set_text(tempoStr.str());
            }
        } else if (id == ColumnId::Length) {
            auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
            if (label) {
                label->set_text(std::to_string(m->length));
            }
        } else if (id == ColumnId::TimeSignature) {
            auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
            if (label) {
                label->set_text(std::to_string(m->timeSignature.numerator) + "/" + std::to_string(m->timeSignature.denominator));
            }
        } else if (id == ColumnId::Intensity) {
            auto progressBar = dynamic_cast<Gtk::ProgressBar*>(list_item->get_child());
            if (progressBar) {
                progressBar->set_fraction(m->intensity);
            }
        } else if (id == ColumnId::Header) {
            auto label = dynamic_cast<Gtk::Label*>(list_item->get_child());
            if (label) {
                label->set_text(m->header);
            }
        }

        auto source = Gtk::DragSource::create();
        source->set_actions(Gdk::DragAction::COPY);
        source->signal_prepare().connect(
            [this, m] (double x, double y) -> Glib::RefPtr<Gdk::ContentProvider> {
                return onBeatDragPrepareData(m->filepath, x, y);
            }, false);
        list_item->get_child()->add_controller(source);
    }
}

void BeatColumnView::onActivate(guint pos)
{
    if ((library != nullptr) && (player != nullptr)) {
        auto i = library->getAllBeats().begin();
        std::advance(i, pos);
        if (!player->play(**i)) {
            std::cerr << "Couldn't play " << (*i)->getFilePath() << std::endl;
        }
    }
}

Glib::RefPtr<Gdk::ContentProvider> BeatColumnView::onBeatDragPrepareData(const std::filesystem::path& filepath, double, double)
{
   std::vector<Glib::RefPtr<Gio::File>> vec;
   vec.push_back(Gio::File::create_for_path(filepath));
   auto list = Glib::SListHandler<Glib::RefPtr<Gio::File>>::vector_to_slist(vec);

   Glib::Value_RefPtrBoxed<Gio::File> value;
   value.init(GDK_TYPE_FILE_LIST);
   g_value_set_boxed(value.gobj(), list.data());

   return Gdk::ContentProvider::create(value);
}

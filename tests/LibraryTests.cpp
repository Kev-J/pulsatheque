#include <catch2/catch_test_macros.hpp>

#include "Library.hpp"

#include <algorithm>

TEST_CASE("Create and flush beats", "[Library]") {
    Library lib;
    Beat *b1 = lib.createBeat();
    Beat *b2 = lib.createBeat();
    auto beats = lib.getAllBeats();

    REQUIRE(std::find(beats.begin(), beats.end(), b1) != beats.end());
    REQUIRE(std::find(beats.begin(), beats.end(), b2) != beats.end());

    lib.flushBeats();

    REQUIRE(lib.getAllBeats().empty());
}
